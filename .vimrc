let s:path = fnamemodify(resolve(expand('<sfile>:p')), ':h')

let g:ale_enabled = 0
let g:ale_c_parse_makefile = 1
let g:ale_c_clang_options = '-Wall -g -I -Mj -Iphp7/coc_links/swish-e/include -Iphp7/coc_links/php -Iphp7/coc_links/php/Zend -Iphp7/coc_links/php/main -Iphp7/coc_links/php/include'
