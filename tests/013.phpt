--TEST--
SwishResult properties
--SKIPIF--
<?php include "skipif.inc"; ?>
--FILE--
<?php

$h = new Swish(dirname(__FILE__)."/res/index.swish-e");

$results = $h->query("test");

$result = $results->nextResult();
var_dump($result);
var_dump($result->test);
$a = array();
var_dump($result->$a);

echo "Done\n";
?>
--EXPECTF--
object(SwishResult)#%d (12) {
  ["swishreccount"]=>
  int(1)
  ["swishrank"]=>
  int(1000)
  ["swishfilenum"]=>
  int(1)
  ["swishdbfile"]=>
  string(%d) "%sindex.swish-e"
  ["swishdocpath"]=>
  string(%d) "%stest.html"
  ["swishtitle"]=>
  string(48) "If you are seeing this, the test was successful!"
  ["swishdocsize"]=>
  int(437)
  ["swishlastmodified"]=>
  int(1238896714)
  ["meta1"]=>
  NULL
  ["meta2"]=>
  NULL
  ["meta3"]=>
  NULL
  ["swishdescription"]=>
  string(50) "This is an initial paragraph... This is a number o"
}

Notice: Undefined property: %wSwishResult::$test in %s on line %d
NULL

Notice: Array to string conversion in %s on line %d

Notice: Undefined property: %wSwishResult::$Array in %s on line %d
NULL
Done
