--TEST--
SwishResult::getMetaList() tests
--SKIPIF--
<?php include "skipif.inc"; ?>
--FILE--
<?php

$h = new Swish(dirname(__FILE__)."/res/index.swish-e");

$rs = $h->query("test");

$r = $rs->nextResult();

var_dump($r->getMetaList("test"));
var_dump($r->getMetaList());

echo "Done\n";
?>
--EXPECTF--
Warning: SwishResult::getMetaList() expects exactly 0 parameters, 1 given in %s on line %d
NULL
array(4) {
  [0]=>
  array(3) {
    ["Name"]=>
    string(12) "swishdefault"
    ["Type"]=>
    int(0)
    ["ID"]=>
    int(1)
  }
  [1]=>
  array(3) {
    ["Name"]=>
    string(5) "meta1"
    ["Type"]=>
    int(0)
    ["ID"]=>
    int(10)
  }
  [2]=>
  array(3) {
    ["Name"]=>
    string(5) "meta2"
    ["Type"]=>
    int(0)
    ["ID"]=>
    int(11)
  }
  [3]=>
  array(3) {
    ["Name"]=>
    string(5) "meta3"
    ["Type"]=>
    int(0)
    ["ID"]=>
    int(12)
  }
}
Done
