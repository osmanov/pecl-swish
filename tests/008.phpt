--TEST--
SwishSearch::setLimit() more sophisticated tests
--SKIPIF--
<?php include "skipif.inc"; ?>
--FILE--
<?php

$h = new Swish(dirname(__FILE__)."/res/index.swish-e");

$search = $h->prepare("see");
try {
	$search->setLimit("swishdocsize", 0, 300);
} catch (SwishException $e) {
	var_dump($e->getMessage());
}

$rs = $search->execute();
var_dump($rs->hits);

$search = $h->prepare("see");
try {
	$search->setLimit("swishdocsize", 300, 1000);
} catch (SwishException $e) {
	var_dump($e->getMessage());
}

$rs = $search->execute();
var_dump($rs->hits);

echo "Done\n";
?>
--EXPECTF--
int(4)
int(1)
Done
