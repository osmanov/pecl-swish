--TEST--
Swish::prepare() tests
--SKIPIF--
<?php include "skipif.inc"; ?>
--FILE--
<?php

$h = new Swish(dirname(__FILE__)."/res/index.swish-e");

$a = array(
    '',
    'four',
    'you',
);

foreach ($a as $query) {
    try {
        var_dump($s = $h->prepare($query));
        var_dump($r = $s->execute());
        var_dump($r->hits);
        var_dump($r = $s->execute());
        var_dump($r->hits);
        var_dump($r = $s->execute($query." OR two"));
        var_dump($r->hits);
    } catch (SwishException $e) {
        var_dump($e->getMessage());
    }
}

var_dump($s = $h->prepare());
var_dump($s = $h->prepare("a", "b"));

echo "Done\n";
?>
--EXPECTF--
object(SwishSearch)#%d (0) {
}
string(25) "No search words specified"
object(SwishSearch)#%d (0) {
}
object(SwishResults)#%d (2) {
  ["hits"]=>
  int(1)
  ["indexes"]=>
  array(1) {
    [0]=>
    array(3) {
      ["name"]=>
      string(%d) "%sindex.swish-e"
      ["parsed_words"]=>
      array(1) {
        [0]=>
        string(4) "four"
      }
      ["removed_stop_words"]=>
      array(0) {
      }
    }
  }
}
int(1)
object(SwishResults)#%d (2) {
  ["hits"]=>
  int(1)
  ["indexes"]=>
  array(1) {
    [0]=>
    array(3) {
      ["name"]=>
      string(%d) "%sindex.swish-e"
      ["parsed_words"]=>
      array(1) {
        [0]=>
        string(4) "four"
      }
      ["removed_stop_words"]=>
      array(0) {
      }
    }
  }
}
int(1)
object(SwishResults)#%d (2) {
  ["hits"]=>
  int(2)
  ["indexes"]=>
  array(1) {
    [0]=>
    array(3) {
      ["name"]=>
      string(%d) "%sindex.swish-e"
      ["parsed_words"]=>
      array(3) {
        [0]=>
        string(4) "four"
        [1]=>
        string(2) "or"
        [2]=>
        string(3) "two"
      }
      ["removed_stop_words"]=>
      array(0) {
      }
    }
  }
}
int(2)
object(SwishSearch)#%d (0) {
}
object(SwishResults)#%d (2) {
  ["hits"]=>
  int(5)
  ["indexes"]=>
  array(1) {
    [0]=>
    array(3) {
      ["name"]=>
      string(%d) "%sindex.swish-e"
      ["parsed_words"]=>
      array(1) {
        [0]=>
        string(3) "you"
      }
      ["removed_stop_words"]=>
      array(0) {
      }
    }
  }
}
int(5)
object(SwishResults)#%d (2) {
  ["hits"]=>
  int(5)
  ["indexes"]=>
  array(1) {
    [0]=>
    array(3) {
      ["name"]=>
      string(%d) "%sindex.swish-e"
      ["parsed_words"]=>
      array(1) {
        [0]=>
        string(3) "you"
      }
      ["removed_stop_words"]=>
      array(0) {
      }
    }
  }
}
int(5)
object(SwishResults)#%d (2) {
  ["hits"]=>
  int(6)
  ["indexes"]=>
  array(1) {
    [0]=>
    array(3) {
      ["name"]=>
      string(%d) "%sindex.swish-e"
      ["parsed_words"]=>
      array(3) {
        [0]=>
        string(3) "you"
        [1]=>
        string(2) "or"
        [2]=>
        string(3) "two"
      }
      ["removed_stop_words"]=>
      array(0) {
      }
    }
  }
}
int(6)
object(SwishSearch)#%d (0) {
}

Warning: Swish::prepare() expects at most 1 parameter, 2 given in %s on line %d
NULL
Done
