--TEST--
SwishSearch::setSort() tests
--SKIPIF--
<?php include "skipif.inc"; ?>
--FILE--
<?php

$h = new Swish(dirname(__FILE__)."/res/index.swish-e");

$search = $h->prepare("see");

$search->setSort("swishrank desc");
$rs = $search->execute();

while ($rs && $r = $rs->nextResult()) {
	var_dump($r->swishrank);
	var_dump($r->swishdocpath);
}

echo "=================\n";

$search->setSort("swishrank asc");
$rs = $search->execute();

while ($rs && $r = $rs->nextResult()) {
	var_dump($r->swishrank);
	var_dump($r->swishdocpath);
}

echo "=================\n";

$search->setSort("blah asc");

try {
	$rs = $search->execute();
} catch (SwishException $e) {
	var_dump($e->getMessage());
}

$search->setSort();

echo "Done\n";
?>
--EXPECTF--
int(1000)
string(%d) "%stest_xml.html"
int(932)
string(%d) "%stest_phrase.html"
int(915)
string(%d) "%stest_meta.html"
int(882)
string(%d) "%stest_meta2.html"
int(685)
string(%d) "%stest.html"
=================
int(685)
string(%d) "%stest.html"
int(882)
string(%d) "%stest_meta2.html"
int(915)
string(%d) "%stest_meta.html"
int(932)
string(%d) "%stest_phrase.html"
int(1000)
string(%d) "%stest_xml.html"
=================
string(%d) "Property 'blah' is not defined in index '%sindex.swish-e'"

Warning: SwishSearch::setSort() expects exactly 1 parameter, 0 given in %s009.php on line %d
Done
