--TEST--
SwishResults and SwishResult basic tests
--SKIPIF--
<?php include "skipif.inc"; ?>
--FILE--
<?php

error_reporting(E_ALL);
$h = new Swish(dirname(__FILE__)."/res/index.swish-e");

$results = $h->query("see");

while ($results && $result = $results->nextResult()) {
	var_dump($result);
	var_dump($result->see);
}

echo "Done\n";
?>
--EXPECTF--
object(SwishResult)#%d (12) {
  ["swishreccount"]=>
  int(1)
  ["swishrank"]=>
  int(1000)
  ["swishfilenum"]=>
  int(7)
  ["swishdbfile"]=>
  string(%d) "%sindex.swish-e"
  ["swishdocpath"]=>
  string(%d) "%stest_xml.html"
  ["swishtitle"]=>
  string(62) "If you are seeing this, the METATAG XML search was successful!"
  ["swishdocsize"]=>
  int(159)
  ["swishlastmodified"]=>
  int(1238896714)
  ["meta1"]=>
  NULL
  ["meta2"]=>
  NULL
  ["meta3"]=>
  string(31) "This is metatest3 Just a sample"
  ["swishdescription"]=>
  string(31) "This is metatest3 Just a sample"
}

Notice: Undefined property: SwishResult::$see in %s004.php on line %d
NULL
object(SwishResult)#%d (12) {
  ["swishreccount"]=>
  int(2)
  ["swishrank"]=>
  int(932)
  ["swishfilenum"]=>
  int(6)
  ["swishdbfile"]=>
  string(%d) "%sindex.swish-e"
  ["swishdocpath"]=>
  string(%d) "%stest_phrase.html"
  ["swishtitle"]=>
  string(57) "If you are seeing this, the PHRASE search was successful!"
  ["swishdocsize"]=>
  int(159)
  ["swishlastmodified"]=>
  int(1238896714)
  ["meta1"]=>
  NULL
  ["meta2"]=>
  NULL
  ["meta3"]=>
  NULL
  ["swishdescription"]=>
  string(50) "Once upon time there was three little pigs and the"
}

Notice: Undefined property: SwishResult::$see in %s004.php on line %d
NULL
object(SwishResult)#%d (12) {
  ["swishreccount"]=>
  int(3)
  ["swishrank"]=>
  int(915)
  ["swishfilenum"]=>
  int(4)
  ["swishdbfile"]=>
  string(%d) "%sindex.swish-e"
  ["swishdocpath"]=>
  string(%d) "%stest_meta.html"
  ["swishtitle"]=>
  string(60) "If you are seeing this, the METATAG search 1 was successful!"
  ["swishdocsize"]=>
  int(233)
  ["swishlastmodified"]=>
  int(1238896714)
  ["meta1"]=>
  string(17) "this is metatest1"
  ["meta2"]=>
  NULL
  ["meta3"]=>
  NULL
  ["swishdescription"]=>
  string(8) "Bla, Bla"
}

Notice: Undefined property: SwishResult::$see in %s004.php on line %d
NULL
object(SwishResult)#%d (12) {
  ["swishreccount"]=>
  int(4)
  ["swishrank"]=>
  int(882)
  ["swishfilenum"]=>
  int(5)
  ["swishdbfile"]=>
  string(%d) "%sindex.swish-e"
  ["swishdocpath"]=>
  string(%d) "%stest_meta2.html"
  ["swishtitle"]=>
  string(60) "If you are seeing this, the METATAG search 2 was successful!"
  ["swishdocsize"]=>
  int(264)
  ["swishlastmodified"]=>
  int(1238896714)
  ["meta1"]=>
  NULL
  ["meta2"]=>
  NULL
  ["meta3"]=>
  NULL
  ["swishdescription"]=>
  string(50) "This is metatest2 Bla, bla This is is the DESCRIPT"
}

Notice: Undefined property: SwishResult::$see in %s004.php on line %d
NULL
object(SwishResult)#%d (12) {
  ["swishreccount"]=>
  int(5)
  ["swishrank"]=>
  int(685)
  ["swishfilenum"]=>
  int(1)
  ["swishdbfile"]=>
  string(%d) "%sindex.swish-e"
  ["swishdocpath"]=>
  string(%d) "%stest.html"
  ["swishtitle"]=>
  string(48) "If you are seeing this, the test was successful!"
  ["swishdocsize"]=>
  int(437)
  ["swishlastmodified"]=>
  int(1238896714)
  ["meta1"]=>
  NULL
  ["meta2"]=>
  NULL
  ["meta3"]=>
  NULL
  ["swishdescription"]=>
  string(50) "This is an initial paragraph... This is a number o"
}

Notice: Undefined property: SwishResult::$see in %s004.php on line %d
NULL
Done
