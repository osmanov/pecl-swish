/*
  +----------------------------------------------------------------------+
  | PHP Version 7                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2020 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Authors: Wez Furlong <wez@omniti.com>                                |
  |          Antony Dovgal <tony2001@php.net>                            |
  |          Ruslan Osmanov <osmanov@php.net>                            |
  +----------------------------------------------------------------------+
*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "zend_exceptions.h"

#include "swish-e.h"

#include "php_swish.h"

ZEND_DECLARE_MODULE_GLOBALS(swish)
static PHP_GINIT_FUNCTION(swish);

/* {{{ internal structs */
static zend_class_entry *ce_sw_exception;
static zend_class_entry *ce_sw_handle;
static zend_class_entry *ce_sw_search;
static zend_class_entry *ce_sw_result;
static zend_class_entry *ce_sw_results;

static zend_object_handlers php_sw_results_obj_handlers;
static zend_object_handlers php_sw_result_obj_handlers;
static zend_object_handlers php_sw_handle_obj_handlers;
static zend_object_handlers php_sw_search_obj_handlers;

struct php_sw_handle {
	SW_HANDLE h;
	zend_object std;
};

struct php_sw_search {
	zval refhandle;
	struct php_sw_handle *h;
	SW_SEARCH s;
	zend_object std;
};

struct php_sw_results {
	zval refhandle;
	struct php_sw_handle *h;
	SW_RESULTS r;
	zend_object std;
};

struct php_sw_result {
	zval refhandle;
	struct php_sw_handle *h;
	SW_RESULT r;
	zend_object std;
};
/* }}} */

/* {{{ internal functions */
static inline struct php_sw_handle* php_swish_handle_fetch_object(zend_object *obj) {
	return (struct php_sw_handle*)((char *)obj - XtOffsetOf(struct php_sw_handle, std));
}
static inline struct php_sw_results* php_swish_results_fetch_object(zend_object *obj) {
	return (struct php_sw_results*)((char *)obj - XtOffsetOf(struct php_sw_results, std));
}
static inline struct php_sw_result* php_swish_result_fetch_object(zend_object *obj) {
	return (struct php_sw_result*)((char *)obj - XtOffsetOf(struct php_sw_result, std));
}
static inline struct php_sw_search* php_swish_search_fetch_object(zend_object *obj) {
	return (struct php_sw_search*)((char *)obj - XtOffsetOf(struct php_sw_search, std));
}
#define Z_SWISH_HANDLE_OBJ_P(zv) php_swish_handle_fetch_object(Z_OBJ_P(zv))
#define Z_SWISH_RESULTS_OBJ_P(zv) php_swish_results_fetch_object(Z_OBJ_P(zv))
#define Z_SWISH_RESULT_OBJ_P(zv) php_swish_result_fetch_object(Z_OBJ_P(zv))
#define Z_SWISH_SEARCH_OBJ_P(zv) php_swish_search_fetch_object(Z_OBJ_P(zv))

#define SWISH_HANDLE_OBJ_P(zv) php_swish_handle_fetch_object((zv))
#define SWISH_RESULTS_OBJ_P(zv) php_swish_results_fetch_object((zv))
#define SWISH_RESULT_OBJ_P(zv) php_swish_result_fetch_object((zv))
#define SWISH_SEARCH_OBJ_P(zv) php_swish_search_fetch_object((zv))

static int sw_throw_exception(struct php_sw_handle *h) /* {{{ */
{
	if (SwishError(h->h)) {
		char *msg = SwishLastErrorMsg(h->h);
		if (!msg || *msg == 0) {
			msg = SwishErrorString(h->h);
		}
		if (!msg || *msg == 0) {
			/* this usually should not happen */
			msg = "Unknown error occured. Please report";
		}
		zend_throw_exception_ex(ce_sw_exception, 0, "%s", msg);
		return 1;
	}
	return 0;
}
/* }}} */

#ifndef HAVE_STEMMER_CONSTANTS /* {{{ */
/* if these constants were not exported, define them */
enum {
	STEM_OK,
	STEM_NOT_ALPHA,
	STEM_TOO_SMALL,
	STEM_WORD_TOO_BIG,
	STEM_TO_NOTHING
};
#endif
/* }}} */

static int sw_stemmer_error_to_exception(int error) /* {{{ */
{
	if (error != STEM_OK) {
		char *msg;
		switch(error) {
			case STEM_NOT_ALPHA:
				msg = "Not all letters are alpha";
				break;
			case STEM_TOO_SMALL:
				msg = "The word is too small to be stemmed";
				break;
			case STEM_WORD_TOO_BIG:
				msg = "The word is too big to be stemmed";
				break;
			case STEM_TO_NOTHING:
				msg = "The word was stemmed to empty string";
				break;
			default:
				msg = "Unknown stemming error";
				break;
		}
		zend_throw_exception_ex(ce_sw_exception, 0, "%s", msg);
		return 1;
	}
	return 0;
}
/* }}} */

static void php_sw_header_to_zval(SWISH_HEADER_VALUE value, SWISH_HEADER_TYPE type, zval *retval) /* {{{ */
{
	switch (type) {
		case SWISH_STRING:
			ZVAL_STRING(retval, (char *)value.string);
			break;

		case SWISH_NUMBER:
			ZVAL_LONG(retval, value.number);
			break;

		case SWISH_BOOL:
			ZVAL_BOOL(retval, value.boolean);
			break;

		case SWISH_LIST:
			{
				const char **string_list;
				string_list = value.string_list;
				array_init(retval);

				while (string_list && *string_list)
				{
					add_next_index_string(retval, (char *)(*string_list));
					string_list++;
				}
			}
			break;

		case SWISH_HEADER_ERROR:
			ZVAL_NULL(retval);
			break;

		default:
			ZVAL_NULL(retval);
			break;
	}
}
/* }}} */

static void sw_handle_dtor(zend_object *object) /* {{{ */
{
	struct php_sw_handle *h = SWISH_HANDLE_OBJ_P(object);

	if (h->h) {
		SwishClose(h->h);
		h->h = NULL;
	}

	zend_object_std_dtor(object);
}
/* }}} */

static void sw_search_dtor(zend_object *object) /* {{{ */
{
	struct php_sw_search *s = SWISH_SEARCH_OBJ_P(object);

	if (s->s) {
		Free_Search_Object(s->s);
		s->s = NULL;
	}

	Z_TRY_DELREF_P(&s->refhandle);

	zend_object_std_dtor(object);
}
/* }}} */

static void sw_results_dtor(zend_object *object) /* {{{ */
{
	struct php_sw_results *r = SWISH_RESULTS_OBJ_P(object);

	if (r->r) {
		Free_Results_Object(r->r);
		r->r = NULL;
	}

	Z_TRY_DELREF_P(&r->refhandle);

	zend_object_std_dtor(object);
}
/* }}} */

static void sw_result_dtor(zend_object *object) /* {{{ */
{
	struct php_sw_result *r = SWISH_RESULT_OBJ_P(object);

	Z_TRY_DELREF_P(&r->refhandle);

	zend_object_std_dtor(object);
}
/* }}} */

static zend_object *sw_handle_new(zend_class_entry *ce) /* {{{ */
{
	struct php_sw_handle *intern = ecalloc(1, sizeof(struct php_sw_handle) + zend_object_properties_size(ce));

	zend_object_std_init(&intern->std, ce);
	object_properties_init(&intern->std, ce);

	intern->std.handlers = &php_sw_handle_obj_handlers;

	return &intern->std;
}
/* }}} */

static zend_object *sw_search_new(zend_class_entry *ce) /* {{{ */
{
	struct php_sw_search *intern = ecalloc(1, sizeof(struct php_sw_search) + zend_object_properties_size(ce));

	zend_object_std_init(&intern->std, ce);
	object_properties_init(&intern->std, ce);

	intern->std.handlers = &php_sw_search_obj_handlers;

	return &intern->std;
}
/* }}} */

static zend_object *sw_results_new(zend_class_entry *ce) /* {{{ */
{
	struct php_sw_results *intern = ecalloc(1, sizeof(struct php_sw_results) + zend_object_properties_size(ce));

	zend_object_std_init(&intern->std, ce);
	object_properties_init(&intern->std, ce);

	intern->std.handlers = &php_sw_results_obj_handlers;

	return &intern->std;
}
/* }}} */

static zend_object *sw_result_new(zend_class_entry *ce) /* {{{ */
{
	struct php_sw_result *intern = ecalloc(1, sizeof(struct php_sw_result) + zend_object_properties_size(ce));

	zend_object_std_init(&intern->std, ce);
	object_properties_init(&intern->std, ce);

	intern->std.handlers = &php_sw_result_obj_handlers;

	return &intern->std;
}
/* }}} */

static void php_sw_results_indexes_to_array(struct php_sw_results *r, zval *z_indexes) /* {{{ */
{
	zval index, ztmp, element;
	const char **index_names;
	SWISH_HEADER_VALUE header_value;
	SWISH_HEADER_TYPE  header_type;

	index_names = SwishIndexNames(r->h->h);

	array_init(z_indexes);

	while (index_names && *index_names) {
		array_init(&index);

		ZVAL_STRING(&ztmp, (char *)(*index_names));
		add_assoc_zval(&index, "name", &ztmp);

		header_type = SWISH_LIST;
		header_value = SwishParsedWords(r->r, *index_names);
		php_sw_header_to_zval(header_value, header_type, &element);
		add_assoc_zval(&index, "parsed_words", &element);

		header_type = SWISH_LIST;
		header_value = SwishRemovedStopwords(r->r, *index_names);
		php_sw_header_to_zval(header_value, header_type, &element);
		add_assoc_zval(&index, "removed_stop_words", &element);

		add_next_index_zval(z_indexes, &index);
		index_names++;
	}
}
/* }}} */

static zval *php_sw_results_read_property(zval *object, zval *member, int type, void **cache_slot, zval *rv) /* {{{ */
{
	zval tmp_member;
	zval *retval = NULL;
	struct php_sw_results *r = NULL;

	if (Z_TYPE_P(member) != IS_STRING) {
#if PHP_VERSION_ID >= 70400
		zend_string *str = zval_try_get_string_func(member);
		if (UNEXPECTED(!str)) {
			return &EG(uninitialized_zval);
		}
		ZVAL_STR(&tmp_member, str);
#else
		ZVAL_COPY(&tmp_member, member);
		convert_to_string(&tmp_member);
#endif
		member = &tmp_member;
		cache_slot = NULL;
	}

	r = Z_SWISH_RESULTS_OBJ_P(object);

	retval = rv;

	if (Z_STRLEN_P(member) == (sizeof("hits") - 1) && !memcmp(Z_STRVAL_P(member), "hits", Z_STRLEN_P(member))) {
		ZVAL_LONG(retval, SwishHits(r->r));
	} else if (Z_STRLEN_P(member) == (sizeof("indexes") - 1) && !memcmp(Z_STRVAL_P(member), "indexes", Z_STRLEN_P(member))) {
		php_sw_results_indexes_to_array(r, retval);
	} else {
		retval = zend_std_read_property(object, member, type, cache_slot, rv);
	}

	if (member == &tmp_member) {
		zval_ptr_dtor_str(&tmp_member);
	}

	return retval;
}
/* }}} */

static HashTable *php_sw_results_get_properties(zval *object) /* {{{ */
{
	struct php_sw_results *r = Z_SWISH_RESULTS_OBJ_P(object);
	HashTable *props = zend_std_get_properties(object);
	zval tmp;

	ZVAL_LONG(&tmp, SwishHits(r->r));
	zend_hash_str_update(props, "hits", sizeof("hits") - 1, &tmp);

	php_sw_results_indexes_to_array(r, &tmp);
	zend_hash_str_update(props, "indexes", sizeof("indexes") - 1, &tmp);

	return props;
}

static int php_sw_prop_to_zval(struct php_sw_result *r, char *name, zval *prop) /* {{{ */
{
	int result = SUCCESS;
	PropValue *v;

	v = getResultPropValue(r->r, name, 0);
	if (!v) {
		return FAILURE;
	}

	switch(v->datatype) {
		case PROP_STRING:
			ZVAL_STRING(prop, v->value.v_str);
			break;
		case PROP_INTEGER:
			ZVAL_LONG(prop, v->value.v_int);
			break;
		case PROP_DATE:
			ZVAL_LONG(prop, v->value.v_date);
			break;
		case PROP_FLOAT:
			ZVAL_DOUBLE(prop, v->value.v_float);
			break;
		case PROP_ULONG:
			ZVAL_LONG(prop, v->value.v_ulong);
			break;
		default:
			result = FAILURE;
			break;
	}
	freeResultPropValue(v);

	return result;
}
/* }}}  */

static zval *php_sw_result_read_property(zval *object, zval *member, int type, void **cache_slot, zval *rv) /* {{{ */
{
	zval tmp_member;
	zval *retval = NULL;
	struct php_sw_result *r = NULL;

	if (Z_TYPE_P(member) != IS_STRING) {
#if PHP_VERSION_ID >= 70400
		zend_string *str = zval_try_get_string_func(member);
		if (UNEXPECTED(!str)) {
			return &EG(uninitialized_zval);
		}
		ZVAL_STR(&tmp_member, str);
#else
		ZVAL_COPY(&tmp_member, member);
		convert_to_string(&tmp_member);
#endif
		member = &tmp_member;
		cache_slot = NULL;
	}

	r = Z_SWISH_RESULT_OBJ_P(object);

	retval = rv;

	if (FAILURE == php_sw_prop_to_zval(r, Z_STRVAL_P(member), retval)) {
		retval = zend_std_read_property(object, member, type, cache_slot, rv);
	}

	if (member == &tmp_member) {
		zval_ptr_dtor_str(&tmp_member);
	}

	return(retval);
}
/* }}} */

static HashTable *php_sw_result_get_properties(zval *object) /* {{{ */
{
	char *name;
	zval prop;
	HashTable *props;
	SWISH_META_LIST meta;
	struct php_sw_result *r;

	props = zend_std_get_properties(object);

	r = Z_SWISH_RESULT_OBJ_P(object);

	meta = SwishResultPropertyList(r->r);

	while (meta && *meta) {
		name = (char*)SwishMetaName(*meta);
		if (FAILURE == php_sw_prop_to_zval(r, name, &prop)) {
			ZVAL_NULL(&prop);
		}
		zend_hash_str_update(props, name, strlen(name), &prop);
		meta++;
	}

	return props;
}
/* }}} */

static void php_sw_handle_indexes_to_array(struct php_sw_handle *h, zval *z_indexes) /* {{{ */
{
	zval index, ztmp;
	const char **index_names, **header_names, **curr_header;

	index_names = SwishIndexNames(h->h);
	header_names = SwishHeaderNames(h->h);

	array_init(z_indexes);

	while (index_names && *index_names) {
		array_init(&index);

		ZVAL_STRING(&ztmp, (char *)(*index_names));
		add_assoc_zval(&index, "name", &ztmp);

		array_init(&ztmp);
		curr_header = header_names;
		while (curr_header && *curr_header) {
			zval header;
			SWISH_HEADER_TYPE header_type;
			SWISH_HEADER_VALUE header_value;

			header_value = SwishHeaderValue(h->h, *index_names, *curr_header, &header_type);
			php_sw_header_to_zval(header_value, header_type, &header);
			add_assoc_zval(&ztmp, (char *)(*curr_header), &header);
			curr_header++;
		}

		add_assoc_zval(&index, "headers", &ztmp);
		add_next_index_zval(z_indexes, &index);
		index_names++;
	}
}
/* }}} */

static zval *php_sw_handle_read_property(zval *object, zval *member, int type, void **cache_slot, zval *rv)
{
	zval tmp_member;
	zval *retval = NULL;
	struct php_sw_handle *h = NULL;

	if (Z_TYPE_P(member) != IS_STRING) {
#if PHP_VERSION_ID >= 70400
		zend_string *str = zval_try_get_string_func(member);
		if (UNEXPECTED(!str)) {
			return &EG(uninitialized_zval);
		}
		ZVAL_STR(&tmp_member, str);
#else
		ZVAL_COPY(&tmp_member, member);
		convert_to_string(&tmp_member);
#endif
		member = &tmp_member;
		cache_slot = NULL;
	}

	h = Z_SWISH_HANDLE_OBJ_P(object);

	retval = rv;

	if (Z_STRLEN_P(member) == (sizeof("indexes") - 1) && !memcmp(Z_STRVAL_P(member), "indexes", Z_STRLEN_P(member))) {
		php_sw_handle_indexes_to_array(h, retval);
	} else {
		retval = zend_std_read_property(object, member, type, cache_slot, rv);
	}

	if (member == &tmp_member) {
		zval_ptr_dtor_str(&tmp_member);
	}

	return retval;
}

/* }}} */

static HashTable *php_sw_handle_get_properties(zval *object) /* {{{ */
{
	struct php_sw_handle *h;
	zval z_indexes;
	HashTable *props;

	props = zend_std_get_properties(object);

	h = Z_SWISH_HANDLE_OBJ_P(object);

	php_sw_handle_indexes_to_array(h, &z_indexes);
	zend_hash_str_update(props, "indexes", sizeof("indexes") - 1, &z_indexes);

	return props;
}
/* }}} */

static void fill_property_list(zval *return_value, SWISH_META_LIST meta) /* {{{ */
{
	array_init(return_value);

	while (meta && *meta) {
		zval prop;
		array_init(&prop);

		add_assoc_string(&prop, "Name", (char*)SwishMetaName(*meta));
		add_assoc_long(&prop, "Type", SwishMetaType(*meta));
		add_assoc_long(&prop, "ID", SwishMetaID(*meta));

		add_next_index_zval(return_value, &prop);
		meta++;
	}
}
/* }}} */

/* }}} */

/* {{{ proto void Swish::__construct(string indices)
   Open a swish index */
static PHP_METHOD(Swish, __construct)
{
	zval *object = getThis();
	struct php_sw_handle *h;
	char *indices;
	size_t indices_len;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "s", &indices, &indices_len)) {
		return;
	}

	h = Z_SWISH_HANDLE_OBJ_P(object);

	if (h->h) {
		/* called __construct() twice, bail out */
		return;
	}

	h->h = SwishInit(indices);

	if (sw_throw_exception(h)) {
		return;
	}
}
/* }}} */

/* {{{ proto object Swish::prepare([string query])
   Returns a new search object */
static PHP_METHOD(Swish, prepare)
{
	struct php_sw_handle *h;
	char *query = NULL;
	size_t qlen = 0;
	struct php_sw_search *s;
	SW_SEARCH search;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "|s", &query, &qlen)) {
		return;
	}

	h = Z_SWISH_HANDLE_OBJ_P(getThis());

	search = New_Search_Object(h->h, query);

	if (sw_throw_exception(h)) {
		if (search) {
			Free_Search_Object(search);
		}
		return;
	}

	object_init_ex(return_value, ce_sw_search);
	s = Z_SWISH_SEARCH_OBJ_P(return_value);
	s->s = search;
	s->h = h;
	ZVAL_ZVAL(&s->refhandle, getThis(), 1, 0);
}
/* }}} */

/* {{{ proto object Swish::query(string query)
   Executes a search, returning the result object */
static PHP_METHOD(Swish, query)
{
	char *query = NULL;
	size_t qlen = 0;
	struct php_sw_handle *h;
	struct php_sw_results *r;
	SW_RESULTS results;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "s", &query, &qlen)) {
		return;
	}

	h = Z_SWISH_HANDLE_OBJ_P(getThis());

	results = SwishQuery(h->h, query);

	if (sw_throw_exception(h)) {
		if (results) {
			Free_Results_Object(results);
		}
		return;
	}

	object_init_ex(return_value, ce_sw_results);
	r = Z_SWISH_RESULTS_OBJ_P(return_value);
	r->r = results;
	r->h = h;

	ZVAL_ZVAL(&r->refhandle, getThis(), 1, 0);
}
/* }}} */

/* {{{ proto array Swish::getMetaList(string indexname)
   Returns an array of meta information for a named index */
static PHP_METHOD(Swish, getMetaList)
{
	struct php_sw_handle *h;
	char *index_name;
	size_t len;
	SWISH_META_LIST meta;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "s", &index_name, &len)) {
		return;
	}

	h = Z_SWISH_HANDLE_OBJ_P(getThis());

	meta = SwishMetaList(h->h, index_name);
	fill_property_list(return_value, meta);
}
/* }}} */

/* {{{ proto array Swish::getPropertyList(string indexname)
   Returns an array of property information for a named index */
static PHP_METHOD(Swish, getPropertyList)
{
	struct php_sw_handle *h;
	char *index_name;
	size_t len;
	SWISH_META_LIST meta;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "s", &index_name, &len)) {
		return;
	}

	h = Z_SWISH_HANDLE_OBJ_P(getThis());

	meta = SwishPropertyList(h->h, index_name);
	fill_property_list(return_value, meta);
}
/* }}} */

/* {{{ proto void SwishSearch::setStructure(int structure)
   Sets the "structure" flag in the search object */
static PHP_METHOD(SwishSearch, setStructure)
{
	struct php_sw_search *s;
	zend_long l;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "l", &l)) {
		return;
	}

	s = Z_SWISH_SEARCH_OBJ_P(getThis());
	SwishSetStructure(s->s, l);
}
/* }}} */

/* {{{ proto void SwishSearch::setPhraseDelimiter(string delim)
   Sets the phrase delimiter character, the default is double-quotes */
static PHP_METHOD(SwishSearch, setPhraseDelimiter)
{
	struct php_sw_search *s;
	char *str;
	size_t str_len;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "s", &str, &str_len)) {
		return;
	}

	s = Z_SWISH_SEARCH_OBJ_P(getThis());
	SwishPhraseDelimiter(s->s, *str);
}
/* }}} */

/* {{{ proto void SwishSearch::setSort(string sort)
   Sets the sort order of the results */
static PHP_METHOD(SwishSearch, setSort)
{
	struct php_sw_search *s;
	char *str;
	size_t str_len;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "s", &str, &str_len)) {
		return;
	}

	s = Z_SWISH_SEARCH_OBJ_P(getThis());

	SwishSetSort(s->s, str);
}
/* }}} */

/* {{{ proto void SwishSearch::setLimit(string prop, string low, string hi)
   Sets the limit parameters for a search */
static PHP_METHOD(SwishSearch, setLimit)
{
	struct php_sw_search *s;
	char *prop, *low, *hi;
	size_t pl, ll, hl;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "sss",
				&prop, &pl, &low, &ll, &hi, &hl)) {
		return;
	}

	s = Z_SWISH_SEARCH_OBJ_P(getThis());
	if (SwishSetSearchLimit(s->s, prop, low, hi) == 0) {
		if (sw_throw_exception(s->h)) {
			return;
		}
	}
}
/* }}} */

/* {{{ proto void SwishSearch::resetLimit()
   Resets the limit parameters for a search */
static PHP_METHOD(SwishSearch, resetLimit)
{
	struct php_sw_search *s;

	if (zend_parse_parameters_none() == FAILURE) {
		return;
	}

	s = Z_SWISH_SEARCH_OBJ_P(getThis());
	SwishResetSearchLimit(s->s);
}
/* }}} */

/* {{{ proto object SwishSearch::execute([string query])
   Returns a new results object */
static PHP_METHOD(SwishSearch, execute)
{
	char *query = NULL;
	size_t qlen = 0;
	struct php_sw_search *s;
	struct php_sw_results *r;
	SW_RESULTS results;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "|s", &query, &qlen)) {
		return;
	}

	s = Z_SWISH_SEARCH_OBJ_P(getThis());

	results = SwishExecute(s->s, query);

	if (sw_throw_exception(s->h)) {
		if (results) {
			Free_Results_Object(results);
		}
		return;
	}

	object_init_ex(return_value, ce_sw_results);
	r = Z_SWISH_RESULTS_OBJ_P(return_value);
	r->r = results;
	r->h = s->h;

	ZVAL_ZVAL(&r->refhandle, &s->refhandle, 1, 0);
}
/* }}} */

/* {{{ proto long SwishResults::seekResult(int position)
   Sets the current seek position in the list of results. 0 is the first. */
static PHP_METHOD(SwishResults, seekResult)
{
	struct php_sw_results *r;
	zend_long l;
	int retval;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "l", &l)) {
		return;
	}

	if (l < 0) {
		zend_throw_exception_ex(ce_sw_exception, 0, "position cannot be less than zero");
		return;
	}

	r = Z_SWISH_RESULTS_OBJ_P(getThis());
	if (!r->r) {
		zend_throw_exception_ex(ce_sw_exception, 0, "no more results");
		return;
	}

	retval = SwishSeekResult(r->r, l);
	if (sw_throw_exception(r->h)) {
		return;
	}
	RETURN_LONG(retval);
}
/* }}} */

/* {{{ proto object SwishResults::nextResult()
   Advances to the next result. Returns false when there are no more results. */
static PHP_METHOD(SwishResults, nextResult)
{
	struct php_sw_results *r;
	struct php_sw_result *result;
	SW_RESULT res;

	if (FAILURE == zend_parse_parameters_none()) {
		return;
	}

	r = Z_SWISH_RESULTS_OBJ_P(getThis());
	if (!r->r) {
		RETURN_FALSE;
	}
	res = SwishNextResult(r->r);
	if (!res) {
		RETURN_FALSE;
	}

	object_init_ex(return_value, ce_sw_result);
	result = Z_SWISH_RESULT_OBJ_P(return_value);
	result->r = res;
	result->h = r->h;

	ZVAL_ZVAL(&result->refhandle, &r->refhandle, 1, 0);
}
/* }}} */

/* {{{ proto array SwishResults::getParsedWords(string indexname)
   Returns the tokenized query */
static PHP_METHOD(SwishResults, getParsedWords)
{
	struct php_sw_results *r;
	SWISH_HEADER_VALUE value;
	char *index;
	size_t index_len;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "s", &index, &index_len)) {
		return;
	}

	r = Z_SWISH_RESULTS_OBJ_P(getThis());
	if (!r->r) {
		RETURN_FALSE;
	}

	value = SwishParsedWords(r->r, index);
	if (!value.string_list) {
		RETURN_FALSE;
	}

	php_sw_header_to_zval(value, SWISH_LIST, return_value);
}
/* }}} */

/* {{{ proto array SwishResults::getRemovedStopwords(string indexname)
   Returns a list of stopwords removed from the query */
static PHP_METHOD(SwishResults, getRemovedStopwords)
{
	struct php_sw_results *r;
	SWISH_HEADER_VALUE value;
	char *index;
	size_t index_len;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "s", &index, &index_len)) {
		return;
	}

	r = Z_SWISH_RESULTS_OBJ_P(getThis());
	if (!r->r) {
		RETURN_FALSE;
	}

	value = SwishRemovedStopwords(r->r, index);
	if (!value.string_list) {
		RETURN_FALSE;
	}

	php_sw_header_to_zval(value, SWISH_LIST, return_value);
}
/* }}} */

/* {{{ proto array SwishResult::getMetaList()
   Returns an array of meta information for the result */
static PHP_METHOD(SwishResult, getMetaList)
{
	struct php_sw_result *r;
	SWISH_META_LIST meta;

	if (FAILURE == zend_parse_parameters_none()) {
		return;
	}

	r = Z_SWISH_RESULT_OBJ_P(getThis());

	meta = SwishResultMetaList(r->r);
	fill_property_list(return_value, meta);
}
/* }}} */

/* {{{ proto array SwishResult::stem(string word)
   Stems the word and returns result as an array */
static PHP_METHOD(SwishResult, stem)
{
	struct php_sw_result *r;
	SW_FUZZYWORD fuzzy_word;
	const char** word_list;
	char *word;
	size_t word_len;
	zval tmp;
	int error;

	if (FAILURE == zend_parse_parameters(ZEND_NUM_ARGS(), "s", &word, &word_len)) {
		return;
	}

	r = Z_SWISH_RESULT_OBJ_P(getThis());

	fuzzy_word = SwishFuzzyWord(r->r, word);
	if (sw_throw_exception(r->h)) {
		return;
	}

	if (!fuzzy_word) {
		RETURN_FALSE;
	}

	word_list = SwishFuzzyWordList(fuzzy_word);
	error = SwishFuzzyWordError(fuzzy_word);
	if (error) {
		sw_stemmer_error_to_exception(error);
		SwishFuzzyWordFree(fuzzy_word);
		RETURN_FALSE;
	}

	array_init(return_value);
	while (word_list && *word_list) {
		ZVAL_STRING(&tmp, (char *)(*word_list));
		add_next_index_zval(return_value, &tmp);

		word_list++;
	}
	SwishFuzzyWordFree(fuzzy_word);
}
/* }}} */

/* {{{ class method tables */

ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_handle__construct, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, indices, IS_STRING, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_handle_prepare, 0, 0, 0)
	ZEND_ARG_TYPE_INFO(0, query, IS_STRING, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_handle_query, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, query, IS_STRING, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_handle_get_meta_list, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, indexname, IS_STRING, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_handle_get_property_list, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, indexname, IS_STRING, 0)
ZEND_END_ARG_INFO();

ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_search_set_structure, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, structure, IS_LONG, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_search_set_phrase_delimiter, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, delim, IS_STRING, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_search_set_sort, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, sort, IS_STRING, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_search_set_limit, 0, 0, 3)
	ZEND_ARG_TYPE_INFO(0, prop, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, low, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, hi, IS_STRING, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO(arginfo_sw_search_reset_limit, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_search_execute, 0, 0, 0)
	ZEND_ARG_TYPE_INFO(0, query, IS_STRING, 0)
ZEND_END_ARG_INFO();

ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_results_seek_result, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, position, IS_LONG, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO(arginfo_sw_results_next_result, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_results_get_parsed_words, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, indexname, IS_STRING, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_results_get_removed_stopwords, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, indexname, IS_STRING, 0)
ZEND_END_ARG_INFO();

ZEND_BEGIN_ARG_INFO(arginfo_sw_result_get_meta_list, 0)
ZEND_END_ARG_INFO();
ZEND_BEGIN_ARG_INFO_EX(arginfo_sw_result_stem, 0, 0, 1)
	ZEND_ARG_TYPE_INFO(0, word, IS_STRING, 0)
ZEND_END_ARG_INFO();

static zend_function_entry sw_handle_methods[] = {
	PHP_ME(Swish, __construct, arginfo_sw_handle__construct, ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
	PHP_ME(Swish, prepare, arginfo_sw_handle_prepare, ZEND_ACC_PUBLIC)
	PHP_ME(Swish, query, arginfo_sw_handle_query, ZEND_ACC_PUBLIC)
	PHP_ME(Swish, getMetaList, arginfo_sw_handle_get_meta_list, ZEND_ACC_PUBLIC)
	PHP_ME(Swish, getPropertyList, arginfo_sw_handle_get_property_list, ZEND_ACC_PUBLIC)
	PHP_FE_END
};

static zend_function_entry sw_search_methods[] = {
	PHP_ME(SwishSearch, execute, arginfo_sw_search_execute, ZEND_ACC_PUBLIC)
	PHP_ME(SwishSearch, setStructure, arginfo_sw_search_set_structure, ZEND_ACC_PUBLIC)
	PHP_ME(SwishSearch, setPhraseDelimiter, arginfo_sw_search_set_phrase_delimiter, ZEND_ACC_PUBLIC)
	PHP_ME(SwishSearch, setSort, arginfo_sw_search_set_sort, ZEND_ACC_PUBLIC)
	PHP_ME(SwishSearch, setLimit, arginfo_sw_search_set_limit, ZEND_ACC_PUBLIC)
	PHP_ME(SwishSearch, resetLimit, arginfo_sw_search_reset_limit, ZEND_ACC_PUBLIC)
	PHP_FE_END
};

static zend_function_entry sw_results_methods[] = {
	PHP_ME(SwishResults, seekResult, arginfo_sw_results_seek_result, ZEND_ACC_PUBLIC)
	PHP_ME(SwishResults, nextResult, arginfo_sw_results_next_result, ZEND_ACC_PUBLIC)
	PHP_ME(SwishResults, getParsedWords, arginfo_sw_results_get_parsed_words, ZEND_ACC_PUBLIC)
	PHP_ME(SwishResults, getRemovedStopwords, arginfo_sw_results_get_removed_stopwords, ZEND_ACC_PUBLIC)
	PHP_FE_END
};

static zend_function_entry sw_result_methods[] = {
	PHP_ME(SwishResult, getMetaList, arginfo_sw_result_get_meta_list, ZEND_ACC_PUBLIC)
	PHP_ME(SwishResult, stem, arginfo_sw_result_stem, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
/* }}} */

/* {{{ swish_functions[] */
static zend_function_entry swish_functions[] = {
	PHP_FE_END
};
/* }}} */

#ifdef COMPILE_DL_SWISH
#ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE();
#endif
ZEND_GET_MODULE(swish)
#endif

/* {{{ PHP_GINIT_FUNCTION */
static PHP_GINIT_FUNCTION(swish)
{
#if defined(COMPILE_DL_SWISH) && defined(ZTS)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif
}
/* }}} */

#define PHP_SWISH_CLASS_CONST(name, value) \
	zend_declare_class_constant_long(ce_sw_handle, name, sizeof(name) - 1, (zend_long)value)

/* {{{ PHP_MINIT_FUNCTION */
PHP_MINIT_FUNCTION(swish)
{
	zend_class_entry ce;

	memcpy(&php_sw_result_obj_handlers, zend_get_std_object_handlers(), sizeof(zend_object_handlers));
	php_sw_result_obj_handlers.offset = XtOffsetOf(struct php_sw_result, std);
	php_sw_result_obj_handlers.clone_obj = NULL;
	php_sw_result_obj_handlers.free_obj = sw_result_dtor;
	php_sw_result_obj_handlers.dtor_obj = zend_objects_destroy_object;
	php_sw_result_obj_handlers.read_property = php_sw_result_read_property;
	php_sw_result_obj_handlers.get_properties = php_sw_result_get_properties;

	memcpy(&php_sw_handle_obj_handlers, zend_get_std_object_handlers(), sizeof(zend_object_handlers));
	php_sw_handle_obj_handlers.offset = XtOffsetOf(struct php_sw_handle, std);
	php_sw_handle_obj_handlers.free_obj = sw_handle_dtor;
	php_sw_handle_obj_handlers.dtor_obj = zend_objects_destroy_object;
	php_sw_handle_obj_handlers.clone_obj = NULL;
	php_sw_handle_obj_handlers.read_property = php_sw_handle_read_property;
	php_sw_handle_obj_handlers.get_properties = php_sw_handle_get_properties;

	memcpy(&php_sw_results_obj_handlers, zend_get_std_object_handlers(), sizeof(zend_object_handlers));
	php_sw_results_obj_handlers.offset = XtOffsetOf(struct php_sw_results, std);
	php_sw_results_obj_handlers.clone_obj = NULL;
	php_sw_results_obj_handlers.free_obj = sw_results_dtor;
	php_sw_results_obj_handlers.dtor_obj = zend_objects_destroy_object;
	php_sw_results_obj_handlers.read_property = php_sw_results_read_property;
	php_sw_results_obj_handlers.get_properties = php_sw_results_get_properties;

	memcpy(&php_sw_search_obj_handlers, zend_get_std_object_handlers(), sizeof(zend_object_handlers));
	php_sw_search_obj_handlers.offset = XtOffsetOf(struct php_sw_search, std);
	php_sw_search_obj_handlers.free_obj = sw_search_dtor;
	php_sw_search_obj_handlers.dtor_obj = zend_objects_destroy_object;
	php_sw_search_obj_handlers.clone_obj = NULL;

	INIT_CLASS_ENTRY(ce, "SwishException", NULL);
	ce_sw_exception = zend_register_internal_class_ex(&ce, zend_exception_get_default());

	INIT_CLASS_ENTRY(ce, "Swish", sw_handle_methods);
	ce_sw_handle = zend_register_internal_class(&ce);
	ce_sw_handle->create_object = sw_handle_new;

	INIT_CLASS_ENTRY(ce, "SwishSearch", sw_search_methods);
	ce_sw_search = zend_register_internal_class(&ce);
	ce_sw_search->create_object = sw_search_new;

	INIT_CLASS_ENTRY(ce, "SwishResults", sw_results_methods);
	ce_sw_results = zend_register_internal_class(&ce);
	ce_sw_results->create_object = sw_results_new;

	INIT_CLASS_ENTRY(ce, "SwishResult", sw_result_methods);
	ce_sw_result = zend_register_internal_class(&ce);
	ce_sw_result->create_object = sw_result_new;

	zend_declare_class_constant_stringl(ce_sw_handle,
			"SWISH_VERSION",
			sizeof("SWISH_VERSION") - 1,
			PHP_SWISH_LIBRARY_VERSION,
			sizeof(PHP_SWISH_LIBRARY_VERSION) - 1);

	PHP_SWISH_CLASS_CONST("META_TYPE_UNDEF",  SW_META_TYPE_UNDEF);
	PHP_SWISH_CLASS_CONST("META_TYPE_STRING", SW_META_TYPE_STRING);
	PHP_SWISH_CLASS_CONST("META_TYPE_ULONG",  SW_META_TYPE_ULONG);
	PHP_SWISH_CLASS_CONST("META_TYPE_DATE",   SW_META_TYPE_DATE);

	PHP_SWISH_CLASS_CONST("IN_FILE_BIT",       IN_FILE_BIT);
	PHP_SWISH_CLASS_CONST("IN_TITLE_BIT",      IN_TITLE_BIT);
	PHP_SWISH_CLASS_CONST("IN_HEAD_BIT",       IN_HEAD_BIT);
	PHP_SWISH_CLASS_CONST("IN_BODY_BIT",       IN_BODY_BIT);
	PHP_SWISH_CLASS_CONST("IN_COMMENTS_BIT",   IN_COMMENTS_BIT);
	PHP_SWISH_CLASS_CONST("IN_HEADER_BIT",     IN_HEADER_BIT);
	PHP_SWISH_CLASS_CONST("IN_EMPHASIZED_BIT", IN_EMPHASIZED_BIT);
	PHP_SWISH_CLASS_CONST("IN_META_BIT",       IN_META_BIT);

	PHP_SWISH_CLASS_CONST("IN_FILE",       IN_FILE);
	PHP_SWISH_CLASS_CONST("IN_TITLE",      IN_TITLE);
	PHP_SWISH_CLASS_CONST("IN_HEAD",       IN_HEAD);
	PHP_SWISH_CLASS_CONST("IN_BODY",       IN_BODY);
	PHP_SWISH_CLASS_CONST("IN_COMMENTS",   IN_COMMENTS);
	PHP_SWISH_CLASS_CONST("IN_HEADER",     IN_HEADER);
	PHP_SWISH_CLASS_CONST("IN_EMPHASIZED", IN_EMPHASIZED);
	PHP_SWISH_CLASS_CONST("IN_META",       IN_META);
	PHP_SWISH_CLASS_CONST("IN_ALL",        IN_ALL);

	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION */
PHP_MINFO_FUNCTION(swish)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "swish support", "enabled");
	php_info_print_table_row(2, "extension version", PHP_SWISH_VERSION);
	php_info_print_table_row(2, "swish library version", PHP_SWISH_LIBRARY_VERSION);
	/* DO NOT REMOVE THIS URL!
	 * It is here for license compliance */
	php_info_print_table_row(2, "source available from", "http://swish-e.org");
	php_info_print_table_end();
}
/* }}} */

/* {{{ swish_module_entry */
zend_module_entry swish_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
	STANDARD_MODULE_HEADER,
#endif
	"swish",
	swish_functions,
	PHP_MINIT(swish),
	NULL,
	NULL,
	NULL,
	PHP_MINFO(swish),
	PHP_SWISH_VERSION,
	PHP_MODULE_GLOBALS(swish),  /* globals descriptor */
	PHP_GINIT(swish),           /* globals ctor */
	NULL,                       /* globals dtor */
	NULL,                       /* post deactivate */
	STANDARD_MODULE_PROPERTIES_EX

};
/* }}} */

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
