#!/bin/bash -
set -e
phpize --clean && phpize
#aclocal && libtoolize --force && autoreconf
#./configure --enable-swish-debug --with-swish="$HOME/usr/lib/swish-e/bin/swish-config"
./configure --disable-swish-debug --with-swish="$HOME/usr/lib/swish-e/bin/swish-config"
make clean
make
